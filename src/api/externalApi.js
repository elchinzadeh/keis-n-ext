import service from './service';

export default class ExternalApi {
    static loginForAsanKadr(body) {
        return service.post('auth/loginForAsanKadr', body)
    }
}