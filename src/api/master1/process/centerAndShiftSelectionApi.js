import service from '../../service';

export default class CenterAndShiftSelectionApi {

    static getCenters() {
        return service.get('master1/process/centerAndShiftSelection/getCenters')
    }

    static getShiftsByCenterId(id) {
        return service.get('master1/process/centerAndShiftSelection/getShiftsByCenterId?centerId=' + id)
    }

    static submitCenterAndShift(body) {
        return service.post('master1/process/centerAndShiftSelection/submitCenterAndShift', body)
    }
}