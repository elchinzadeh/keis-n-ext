import axios from 'axios';

const host = window.location.host;
const protocol = window.location.protocol;

const service = axios.create({
    // baseURL: 'http://192.168.20.245:8080/keis-ext/',
    // baseURL: 'http://192.168.35.51:8888/',
    // baseURL: 'http://127.0.0.1:9009/',
    baseURL: protocol + '//' + host + '/api/keis-ext',
    timeout: 10000,
    headers: {
        'Authorization': 'Basic b9626bbfb900498c9158db074f17ac6a',
        'RequestNumber': 'test',
        'AppName': 'keis',
        'IpAddress': '192.168.1.238',
        'Accept-Language': 'az',
        'UserGroupTypeId': 1,
        'Content-Type': 'application/json',
        "X-Requested-With": "XMLHttpRequest",
    }
});

export default service;