import service from '../service';

export default class ProfileApi {

    static getPersonalInfo() {
        return service.get('profile/personalInfo/get');
    }

    static updatePersonalInfo(body) {
        return service.put('profile/personalInfo/updatePersonalInfo', body);
    }

    static getCountryList() {
        return service.get('helpers/listCountry/getAll');
    }

}