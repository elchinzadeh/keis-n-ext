import service from '../service'

export default class FamilyApi {

    static getFamilyInfo() {
        return service.get('profile/familyInfo/get')
    }

    static insertFamilyInfo(body) {
        return service.post('profile/familyInfo/insert', body)
    }

    static updateFamilyInfo(body) {
        return service.put('profile/familyInfo/update', body)
    }

    static deleteFamilyInfo(id) {
        return service.delete('profile/familyInfo/delete/'+id)
    }
}