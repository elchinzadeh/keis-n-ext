import service from '../service';

export default class EducationApi {

    static getEducationInfo() {
        return service.get('profile/eduInfo/get');
    }

    static insertEducationInfo(body) {
        return service.post('profile/eduInfo/insert', body);
    }

    static updateEducationInfo(body) {
        return service.put('profile/eduInfo/update', body);
    }

    static deleteEducationInfo(id) {
        return service.delete('profile/eduInfo/delete/'+id);
    }
}