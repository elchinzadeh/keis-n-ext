import service from '../service';

export default class SkillApi {

    static get() {
        return service.get('profile/skillInfo/get')
    }

    static getItSkillById(id) {
        return service.get('profile/skillInfo/getItSkillById/' + id)
    }

    static getLangSkillById(id) {
        return service.get('profile/skillInfo/getLangSkillById/' + id)
    }

    static getSportSkillById(id) {
        return service.get('profile/skillInfo/getSportSkillById/' + id)
    }

    static getMusicSkillById(id) {
        return service.get('profile/skillInfo/getMusicSkillById/' + id)
    }

    static getOtherSkillById(id) {
        return service.get('profile/skillInfo/getOtherSkillById/' + id)
    }

    static insertItSkill(body) {
        return service.post('profile/skillInfo/insertItSkill/', body)
    }

    static insertLangSkill(body) {
        return service.post('profile/skillInfo/insertLangSkill/', body)
    }

    static insertSportSkill(body) {
        return service.post('profile/skillInfo/insertSportSkill/', body)
    }

    static insertMusicSkill(body) {
        return service.post('profile/skillInfo/insertMusicSkill/', body)
    }

    static insertOtherSkill(body) {
        return service.post('profile/skillInfo/insertOtherSkill/', body)
    }

    static updateItSkill(body) {
        return service.put('profile/skillInfo/updateItSkill/', body)
    }

    static updateLangSkill(body) {
        return service.put('profile/skillInfo/updateLangSkill/', body)
    }

    static updateSportSkill(body) {
        return service.put('profile/skillInfo/updateSportSkill/', body)
    }

    static updateMusicSkill(body) {
        return service.put('profile/skillInfo/updateMusicSkill/', body)
    }

    static updateOtherSkill(body) {
        return service.put('profile/skillInfo/updateOtherSkill/', body)
    }

    static delete(id) {
        return service.delete('profile/skillInfo/delete/' + id)
    }
}