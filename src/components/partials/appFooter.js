import React from 'react';
import {Layout} from 'antd';
import './appFooter.css';

const {Footer} = Layout;

const AppFooter = (props) => {

    return (
        <Footer className={'footer'}>
            Könüllülük Prosesinin Elektron İdarəetmə Sistemi ©2018 ASAN Xidmət
        </Footer>
    );

};

export default AppFooter;