import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {Layout, Icon, Popover, Divider} from 'antd';
import logo from '../../logo-w.png';

import './appHeader.css';

const {Header} = Layout;

class AppHeader extends Component {


    render() {

        const content = (
            <div className="topnav-mobile">
                <NavLink className="nav-link-mobile" to="/dashboard" activeClassName="active" exact>
                    Lövhə
                </NavLink>
                <NavLink className="nav-link-mobile" to="/profile" activeClassName="active">
                    Profil
                </NavLink>
                <NavLink className="nav-link-mobile" to="/attendance">
                    Davamiyyət
                </NavLink>
                <Divider/>
                <NavLink className="nav-link-mobile" to="/kadr">
                    ASAN Kadr
                </NavLink>
            </div>
    );

        return (
            <Header style={{background: '#fff'}}>

                <NavLink to="/">
                    <img className="logo" src={logo} alt="..."/>
                </NavLink>

                <div className="topnav">

                    <NavLink className="nav-link" to="/dashboard" activeClassName="active" exact>
                        Lövhə
                    </NavLink>
                    <NavLink className="nav-link" to="/profile" activeClassName="active">
                        Profil
                    </NavLink>
                    <NavLink className="nav-link" to="/attendance">
                        Davamiyyət
                    </NavLink>
                    <NavLink className="nav-link external-link" to="/kadr">
                        ASAN Kadr
                    </NavLink>
                    <Popover placement="bottom" content={content} trigger="click">
                        <a className="nav-collapse">
                            <Icon type="bars" style={{ fontSize: 20 }}/>
                        </a>
                    </Popover>
                </div>

            </Header>
        );

    }

}

export default AppHeader;