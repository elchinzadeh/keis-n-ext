import React, {Component} from 'react';
import {Card, Alert} from 'antd';

import ProfileApi from '../../../api/profile/profileApi';

import PersonalInfoForm from './personalInfoForm';
import HelperApi from "../../../api/helperApi";

class PersonalInfo extends Component {

    constructor() {
        super();

        this.state = {
            error: null,
            countryList: [],
            districtCityList: [],
            fetching: false,
            posting: false,
            entity: null
        };
    }

    componentDidMount() {
        this.loadPersonalInfo();
        this.loadCountryList();
        this.loadDistrictCityList();
    };

    loadCountryList = () => {

        ProfileApi.getCountryList().then(response => {

            if (response.data.error === null) {

                this.setState({
                    fetching: false,
                    countryList: response.data.data.entities
                });

            } else {

                this.setState({
                    fetching: false,
                    error: response.data.error.message
                });

            }

        }).catch(error => {

            this.setState({
                fetching: false,
                error: error.message
            });

        });

    };

    loadDistrictCityList = () => {

        HelperApi.getDistrictCities().then(response => {
            if (response.data.error === null) {
                this.setState({
                    fetching: false,
                    districtCityList: response.data.data.entities
                });
            } else {
                this.setState({
                    fetching: false,
                    error: response.data.error.message
                });
            }
        }).catch(error => {
            this.setState({
                fetching: false,
                error: error.message
            });
        });
    };

    loadPersonalInfo = () => {

        this.setState({
            fetching: true
        });

        ProfileApi.getPersonalInfo().then(response => {

            if (response.data.error === null) {

                const data = response.data.data;

                this.setState({
                    fetching: false,
                    entity: data
                });

            } else {

                this.setState({
                    fetching: false,
                    error: response.data.error.message
                });

            }

        }).catch(error => {

            this.setState({
                fetching: false,
                error: error.message
            });

        });

    };

    updatePersonalInfo = (values) => {

        this.setState({
            posting: true
        });

        const personalInfo = {
            idCardNo: values.idCardNo,
            idCardFin: values.idCardFin,
            firstName: values.firstName,
            lastName: values.lastName,
            patronymic: values.patronymic,
            birthPlace: values.birthPlace,
            maritalStatus: values.maritalStatus,
            birthDate: values.birthDate,
            citizenshipCountryId: {id: values.citizenshipCountryId},
            sex: values.sex,
            registeredPlaceAddress: values.registeredPlaceAddress,
            livingPlaceAddress: values.livingPlaceAddress,
            nationality: values.nationality,
            physicallyLimitation: values.physicallyLimitation,
            militaryAttitude: values.militaryAttitude,
            haveDriveLicense: values.haveDriveLicense,
            livingPlaceDistrictCityId: {id: values.livingPlaceDistrictCityId},
            registeredPlaceDistrictCityId: {id: values.registeredPlaceDistrictCityId},
            phoneHouse: values.phoneHouse,
            phoneMobile: values.phoneMobile,
            email: values.email,
            facebookProfileUrl: values.facebookProfileUrl
        };

        ProfileApi.updatePersonalInfo(personalInfo).then(response => {

            if (response.data.error === null) {

                this.setState({
                    posting: false
                });

                this.loadPersonalInfo();

            } else {

                this.setState({
                    posting: false,
                    error: response.data.error.message
                });

            }

        }).catch(error => {

            this.setState({
                posting: false,
                error: error.message
            });

        });

    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    handleSave = (e) => {

        e.preventDefault();

        const form = this.formRef.props.form;

        form.validateFields((err, values) => {

            console.log(err, values);

            if (err) {
                return;
            }

            this.updatePersonalInfo(values);

        });
    };

    render() {

        return (
            <Card loading={this.state.fetching}>

                {this.state.error !== null ? <Alert message={this.state.error} style={{marginBottom: '2em'}} type="error" showIcon banner/> : null}

                <PersonalInfoForm countries={this.state.countryList}
                                  districtCities={this.state.districtCityList}
                                  posting={this.state.posting}
                                  handleSave={this.handleSave}
                                  wrappedComponentRef={this.saveFormRef}
                                  entity={this.state.entity} />

            </Card>
        );

    }
}

export default PersonalInfo;