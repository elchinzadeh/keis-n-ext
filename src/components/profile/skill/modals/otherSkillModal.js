import React from 'react';
import {Modal, Form, Input, Select} from 'antd';
import HelperApi from "../../../../api/helperApi";

export class OtherSkillForm extends React.Component{

    constructor() {
        super();

        this.state = {
            listKnowledgeLevel: null
        }
    }

    componentDidMount () {
        this.getKnowledgeLevels()
    }

    componentWillUnmount() {
        this.props.clearSelectedEntity()
    }

    getKnowledgeLevels = () => {
        HelperApi.getKnowledgeLevels().then(response => {
            if (response.data.error === null) {
                this.setState({
                    listKnowledgeLevel: response.data.data.entities
                })
            }
        })
    };

    render() {
        const {selectedEntity, showModal, handleOk, handleCancel, form} = this.props;
        const {getFieldDecorator} = form;
        const formItemLayout = {
            labelCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 6
                },
                lg: {
                    span: 6
                }
            },
            wrapperCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 18
                },
                lg: {
                    span: 18
                }
            }
        };

        return(
            <Modal
                title={selectedEntity ? 'Bilik və bacarıqların redaktə edilməsi' : 'Bilik və bacarıqların əlavə edilməsi'}
                visible={showModal}
                onOk={handleOk}
                onCancel={handleCancel}
            >

                <Form>
                    <Form.Item label="Adı" {...formItemLayout}>
                        {getFieldDecorator('skillName', {
                            initialValue: selectedEntity !== null ? selectedEntity.skillName : null,
                            rules: [{required: true, message: 'Zəhmət olmasa bacarıq adını daxil edin'}],
                        })(
                            <Input placeholder="Bilik və ya bacarıq adı" />
                        )}
                    </Form.Item>

                    <Form.Item label="Dərəcəsi" {...formItemLayout}>
                        {getFieldDecorator('knowledgeLevelId', {
                            initialValue: selectedEntity ? selectedEntity.knowledgeLevelId.id : null,
                            rules: [{required: true, message: 'Zəhmət olmasa bacarıq səviyyəsini daxil edin'}],
                        })(
                            <Select placeholder="Seçim edin"
                                    style={{width: '100%'}}>
                                {this.state.listKnowledgeLevel ?
                                    this.state.listKnowledgeLevel.map(item => {
                                        return <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                    }) : null
                                }
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Qeyd" {...formItemLayout}>
                        {getFieldDecorator('note', {
                            initialValue: selectedEntity !== null ? selectedEntity.note : null,
                            rules: [{required: false}],
                        })(
                            <Input.TextArea placeholder="Qeyd" autosize={{ minRows: 2, maxRows: 6 }} />
                        )}
                    </Form.Item>
                </Form>

            </Modal>
        )
    }

}

const OtherSkillModal = Form.create()(OtherSkillForm);

export default OtherSkillModal;