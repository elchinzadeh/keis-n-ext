import React from 'react';
import {Modal, Form, Input, Select} from 'antd';
import HelperApi from "../../../../api/helperApi";

export class LanguageSkillForm extends React.Component{

    constructor() {
        super();

        this.state = {
            listLanguage: null,
            listKnowledgeLevel: null
        }
    }

    componentDidMount () {
        this.getListLanguage();
        this.getKnowledgeLevels();
    }

    componentWillUnmount() {
        this.props.clearSelectedEntity()
    }

    getListLanguage = () => {
        HelperApi.getListLanguage().then(response => {
            if (response.data.error === null) {
                this.setState({
                    listLanguage: response.data.data.entities
                })
            }
        })
    };

    getKnowledgeLevels = () => {
        HelperApi.getKnowledgeLevels().then(response => {
            if (response.data.error === null) {
                this.setState({
                    listKnowledgeLevel: response.data.data.entities
                })
            }
        })
    };

    render() {
        const {selectedEntity, showModal, handleOk, handleCancel, form} = this.props;
        const {getFieldDecorator} = form;
        const formItemLayout = {
            labelCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 6
                },
                lg: {
                    span: 6
                }
            },
            wrapperCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 18
                },
                lg: {
                    span: 18
                }
            }
        };

        return(
            <Modal
                title={selectedEntity ? 'Dil biliklərinin redaktə edilməsi' : 'Dil biliklərinin əlavə edilməsi'}
                visible={showModal}
                onOk={handleOk}
                onCancel={handleCancel}
            >

                <Form>
                    <Form.Item label="Adı" {...formItemLayout}>
                        {getFieldDecorator('languageId', {
                            initialValue: selectedEntity ? selectedEntity.languageId.id : null,
                            rules: [{required: true, message: 'Zəhmət olmasa dili seçin'}],
                        })(
                            <Select showSearch={true}
                                    optionFilterProp="children"
                                    placeholder="Seçim edin"
                                    style={{width: '100%'}}>
                                {this.state.listLanguage ?
                                    this.state.listLanguage.map(item => {
                                        return <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                    }) : null
                                }
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Dərəcəsi" {...formItemLayout}>
                        {getFieldDecorator('knowledgeLevelId', {
                            initialValue: selectedEntity ? selectedEntity.knowledgeLevelId.id : null,
                            rules: [{required: true, message: 'Zəhmət olmasa bacarıq səviyyəsini daxil edin'}],
                        })(
                            <Select placeholder="Seçim edin"
                                    style={{width: '100%'}}>
                                {this.state.listKnowledgeLevel ?
                                    this.state.listKnowledgeLevel.map(item => {
                                        return <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                    }) : null
                                }
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Qeyd" {...formItemLayout}>
                        {getFieldDecorator('note', {
                            initialValue: selectedEntity !== null ? selectedEntity.note : null,
                            rules: [{required: false}],
                        })(
                            <Input.TextArea placeholder="Qeyd" autosize={{ minRows: 2, maxRows: 6 }} />
                        )}
                    </Form.Item>
                </Form>

            </Modal>
        )
    }

}

const LanguageSkillModal = Form.create()(LanguageSkillForm);

export default LanguageSkillModal;