import React, {Component} from 'react'
import {Modal, Form, Input, Select, DatePicker} from 'antd';
import moment from 'moment'

const FormItem = Form.Item;
const Option = Select.Option;

class FamForm extends Component{
    render() {
        const {familyInfo, kinshipTypes, visible, onCancel, onCreate, form} = this.props;
        const {getFieldDecorator} = form;
        const formItemLayout = {
            labelCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 6
                },
                lg: {
                    span: 6
                }
            },
            wrapperCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 18
                },
                lg: {
                    span: 18
                }
            }
        };

        return(
            <Modal
                title={familyInfo === null ? "Ailə məlumatlarının əlavə edilməsi" : "Ailə məlumatlarının redaktə edilməsi"}
                visible={visible}
                onOk={onCreate}
                onCancel={onCancel}
                okText="Təsdiq"
                cancelText="İmtina"
            >
                <Form layout="horizontal">
                    <FormItem label="Adı" {...formItemLayout}>
                        {getFieldDecorator('nameSurname', {
                            initialValue: familyInfo !== null ? familyInfo.nameSurname : null,
                            rules: [{ required: true, message: 'Zəhmət olmasa ad, soyad, ata adını daxil edin' }],
                        })(
                            <Input placeholder="Ad, Soyad, Ata adı" />
                        )}
                    </FormItem>

                    <FormItem label="Qohumluq növü" {...formItemLayout}>
                        {getFieldDecorator('kinshipTypeId', {
                            initialValue: familyInfo !== null ? familyInfo.kinshipTypeId.id : null,
                            rules: [{ required: true, message: 'Zəhmət olmasa qohumluq növünü seçin' }],
                        })(
                            <Select
                                showSearch
                                placeholder="Qohumluq növünü seçin"
                                optionFilterProp="name"
                            >{
                                kinshipTypes.map((item) => {
                                    return <Option key={item.id} value={item.id} name={item.name} >{item.name}</Option>
                                })
                            }
                            </Select>
                        )}
                    </FormItem>

                    <FormItem label="Doğum yeri" {...formItemLayout}>
                        {getFieldDecorator('birthPlace', {
                            initialValue: familyInfo !== null ? familyInfo.birthPlace : null,
                            rules: [{ required: true, message: 'Zəhmət olmasa doğum yerini daxil edin' }],
                        })(
                            <Input placeholder="Doğum yeri" />
                        )}
                    </FormItem>

                    <FormItem label="Doğum tarixi" {...formItemLayout}>
                        {getFieldDecorator('birthDate', {
                            initialValue: familyInfo !== null ? moment(familyInfo.birthDate) : null,
                            rules: [{ required: true, message: 'Zəhmət olmasa doğum tarixini daxil edin' }],
                        })(
                            <DatePicker/>
                        )}
                    </FormItem>

                    <FormItem label="İşlədiyi yer" {...formItemLayout}>
                        {getFieldDecorator('jobInformation', {
                            initialValue: familyInfo !== null ? familyInfo.jobInformation : null,
                            rules: [{ required: true, message: 'Zəhmət olmasa işlədiyi yer və vəzifəsini daxil edin' }],
                        })(
                            <Input placeholder="İşlədiyi yer və vəzifəsi" />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

const FamilyForm = Form.create()(FamForm);

export default FamilyForm;