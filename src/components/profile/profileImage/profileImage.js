import React, { Component } from 'react';
import {Button} from 'antd'

import './profileImage.css';
import ProfileApi from "../../../api/profile/profileApi";
import ProfileImageUpload from './profileImageUpload'

export default class PrifileImage extends Component {

    constructor() {
        super();

        this.state = {
            image: '',
            showModal: false
        }
    }

    componentDidMount() {
        this.getProfileImage()
    }

    getProfileImage = () => {
        ProfileApi.getPersonalInfo().then(response => {
            if (response.data.error === null) {
                this.setState({
                    image: response.data.data.image
                })
            }
        })
    };

    showModal = () => {
        this.setState({
            showModal: true
        })
    };

    hideModal = () => {
        this.setState({
            showModal: false
        })
    };

    render() {
        return(
            <div className="profile-image">
                <ProfileImageUpload visible={this.state.showModal}
                                    image={this.state.image}
                                    handleCancel={this.hideModal}
                />
                <div className="image" style={{backgroundImage: 'url("'+this.state.image+'")' }} />

                <div className="edit-btn">
                    <Button type="normal" shape="circle" icon="edit" size={'small'} onClick={this.showModal} />
                </div>

            </div>
        )
    }
}

//https://netbranding.co.nz/wp-content/uploads/2014/05/avatar-1.png

