import React from 'react';
import {Button, Modal} from 'antd';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';



export default class ProfileImageUpload extends React.Component {

    constructor() {
        super();

        this.state = {
            crop: {
                x: 10,
                y: 10,
                width: 80,
                height: 80,
            },
        }
    }

    componentDidMount() {
        this.onImageLoaded(this.props.image)
    }

    onImageLoaded = image => {
        console.log('onCropComplete', image)
    };

    onCropComplete = crop => {
        console.log('onCropComplete', crop)
    };

    onCropChange = crop => {
        this.setState({ crop })
    };

    render() {
        console.log(this.state);
        const {visible, handleOk, handleCancel} = this.props;

        return(
            <Modal
                title="Basic Modal"
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <ReactCrop
                    src={this.props.image}
                    crop={this.state.crop}
                    onImageLoaded={this.onImageLoaded}
                    onComplete={this.onCropComplete}
                    onChange={this.onCropChange}
                />

                <Button>Click</Button>
            </Modal>
        )
    }
}