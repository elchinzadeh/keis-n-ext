import React, {Component} from 'react';
import {Col, Row, Card, List, Button, Icon, Popconfirm, message, Divider} from 'antd'
import * as moment from 'moment';

import ExperienceForm from './experienceForm'

import ExperienceApi from "../../../api/profile/experienceApi";

export default class ExperienceInfo extends Component {

    constructor() {
        super();
        this.state = {
            fetching: false,
            modalOpen: false,
            entities: [],
            selectedEntity: null,
            totalCount: 0,
            areas: []
        }
    }

    componentDidMount() {
        this.loadExperienceInfo();
    }

    loadExperienceInfo = () => {
        this.setState({
            fetching: true
        });

        ExperienceApi.getExperienceInfo().then(response => {
            this.setState({
                fetching: false
            });

            if (response.data.error == null) {
                this.setState({
                    entities: response.data.data.entities,
                    totalCount: response.data.data.totalCount
                });
            }else{
                message.error(response.data.error)
            }
        }).catch(error => {
            this.setState({
                fetching: false
            });

            message.error(error)
        });
    };

    insertExperieceInfo = (values) => {
        const form = this.formRef.props.form;

        const body = {
            organization: values.organization,
            position: values.position,
            startDate: values.dateRange[0].format('YYYY-MM-DD'),
            endDate: values.dateRange[1].format('YYYY-MM-DD'),
            note: values.note,
            jobAreaId: {id: values.jobAreaId}
        };

        ExperienceApi.insertExperienceInfo(body).then(response => {

            if (response.data.error === null) {
                this.loadExperienceInfo();
                form.resetFields();
                this.setState({modalOpen: false});
            } else {
                message.error(response.data.error.message);
            }

        }).catch();
    };

    editExperienceInfo = (item) => {
        this.setState({
            selectedEntity: item,
            modalOpen: true
        });
    };

    updateExperieceInfo = (values) => {
        const form = this.formRef.props.form;

        const body = {
            id: this.state.selectedEntity.id,
            organization: values.organization,
            position: values.position,
            startDate: values.dateRange[0].format('YYYY-MM-DD'),
            endDate: values.dateRange[1].format('YYYY-MM-DD'),
            note: values.note,
            jobAreaId: {id: values.jobAreaId}
        };

        ExperienceApi.updateExperienceInfo(body).then(response => {

            if (response.data.error === null) {
                this.loadExperienceInfo();
                form.resetFields();
                this.setState({modalOpen: false});
            } else {
                message.error(response.data.error.message);
            }
        }).catch();
    };

    deleteExperienceInfo = (id) => {
        ExperienceApi.deleteExperienceInfo(id).then(response => {

            if (response.data.error === null) {
                this.loadExperienceInfo();
            } else {
                message.error(response.data.error.message);
            }

        }).catch();
    };

    showModal = () => {
        this.setState({
            modalOpen: true,
            selectedEntity: null
        });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    handleCancel = () => {
        this.setState({
            modalOpen: false
        })
    };

    handleCreate = () => {
        const form = this.formRef.props.form;

        form.validateFields((err, values) => {

            if (err) {
                return;
            }

            if (this.state.selectedEntity === null) {
                this.insertExperieceInfo(values);
            } else {
                this.updateExperieceInfo(values);
            }
        });
    };

    render() {
        return (
            <Card loading={this.state.fetching}>
                <Divider>Təcrübə məlumatları</Divider>
                <ExperienceForm wrappedComponentRef={this.saveFormRef}
                                visible={this.state.modalOpen}
                                experienceInfo={this.state.selectedEntity}
                                areas={this.state.areas}
                                onCancel={this.handleCancel}
                                onCreate={this.handleCreate}
                />

                <Row>
                    <Col xs={24} sm={24} md={{span: 14, offset: 5}} lg={{span: 14, offset: 5}} style={{textAlign: 'right'}}>
                        <Button type="primary" onClick={this.showModal}>
                            <Icon type="plus-circle-o"/>Əlavə et
                        </Button>
                    </Col>
                </Row>

                <Row>
                    <Col xs={24} sm={24} md={{span: 14, offset: 5}} lg={{span: 14, offset: 5}}>

                        <List   dataSource={this.state.entities}
                                renderItem={item => (
                                <List.Item actions={[
                                    <a onClick={() => this.editExperienceInfo(item)}>Dəyişiklik et</a>,
                                    <Popconfirm title="Təcrübə məlumatını silməyə əminsinizmi?"
                                                onConfirm={() => this.deleteExperienceInfo(item.id)}
                                                okText="Bəli"
                                                cancelText="Xeyr">
                                        <a style={{color: 'red'}}>Sil</a>
                                    </Popconfirm>
                                ]}>

                                    <List.Item.Meta
                                        title={<a onClick={(e) => { e.preventDefault(); }}>{item.organization}</a>}
                                        description={item.position}
                                    />
                                    <div>
                                        {moment(item.startDate).format('LL')}
                                        &nbsp;-&nbsp;
                                        {item.endDate === null ? 'Davam edir' : moment(item.endDate).format('LL')}
                                    </div>
                                </List.Item>

                            )}
                        />

                    </Col>
                </Row>
            </Card>
        )
    }
}