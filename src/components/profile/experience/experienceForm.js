import React, {Component} from 'react';
import {Modal, Form, DatePicker, Select, Input} from 'antd';

import * as moment from 'moment';

const {RangePicker} = DatePicker;
const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

class ExpForm extends Component {


    render() {
        const {visible, onCancel, onCreate, form, experienceInfo} = this.props;

        const {getFieldDecorator} = form;

            const formItemLayout = {
                labelCol: {
                    sm: {
                        span: 24
                    },
                    xs: {
                        span: 24
                    },
                    md: {
                        span: 6
                    },
                    lg: {
                        span: 6
                    }
                },
                wrapperCol: {
                    sm: {
                        span: 24
                    },
                    xs: {
                        span: 24
                    },
                    md: {
                        span: 18
                    },
                    lg: {
                        span: 18
                    }
                }
            };

        return (
            <Modal
                visible={visible}
                title={experienceInfo === null ? 'Təcrübə məlumatının əlavə edilməsi' : 'Təcrübə məlumatının redaktəsi'}
                okText="Təsdiq"
                cancelText="İmtina"
                onCancel={onCancel}
                onOk={onCreate}>

                <Form layout="horizontal">

                    <FormItem label="Müəssisə adı" {...formItemLayout}>
                        {getFieldDecorator('organization', {
                            initialValue: experienceInfo !== null ? experienceInfo.organization : null,
                            rules: [{ required: true, message: 'Zəhmət olmasa təcrübə keçdiyiniz müəssisəni daxil edin' }],
                        })(
                            <Input placeholder="Müəssisə adı" />
                        )}
                    </FormItem>

                    <FormItem label="Vəzifə" {...formItemLayout}>
                        {getFieldDecorator('position', {
                            initialValue: experienceInfo !== null ? experienceInfo.position : null,
                            rules: [{ required: true, message: 'Zəhmət olmasa vəzifənizi daxil edin' }],
                        })(
                            <Input placeholder="Vəzifə" />
                        )}
                    </FormItem>

                    <FormItem label={"İş kateqoriyası"} {...formItemLayout}>
                        {getFieldDecorator('jobAreaId', {
                            initialValue: experienceInfo !== null ? experienceInfo.jobAreaId.id : null,
                            rules: [{required: true, message: 'Zəhmət olmasa iş kateqoriyasını daxil edin'}]
                        })(
                            <Select
                                showSearch
                                placeholder="İş kateqoriyası"
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            >
                                <Option key={1}>Jack</Option>

                            </Select>
                        )}
                    </FormItem>

                    <FormItem label="Tarix aralığı" {...formItemLayout}>
                        {getFieldDecorator('dateRange', {
                            initialValue: experienceInfo !== null ? [moment(experienceInfo.startDate), moment(experienceInfo.endDate)] : [],
                            rules: [{required: true, message: 'Zəhmət olmasa işlədiyiniz tarix aralığını seçin'}],
                        })(<RangePicker format="DD.MM.YYYY" placeholder={['Başlama tarixi', 'Bitmə tarixi']}/>)}
                    </FormItem>

                    <FormItem label={"Qeyd"} {...formItemLayout}>
                        {getFieldDecorator('note', {
                            initialValue: experienceInfo !== null ? experienceInfo.note : null,
                            rules: [{required: false}]
                        })(
                            <TextArea placeholder="Qeyd" autosize={{ minRows: 1, maxRows: 5 }} />
                        )}
                    </FormItem>


                </Form>

            </Modal>
        );
    }
}

const ExperienceForm = Form.create()(ExpForm);

export default ExperienceForm;