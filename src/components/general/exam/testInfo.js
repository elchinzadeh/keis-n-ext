import React from 'react';
import {Col, Row, Button, Progress} from 'antd';
import moment from "moment/moment";


const TestInfo = (props) => {
    const {duration, timeLeft, totalCount, currentTest, testQuestions, getTest, finishExam} = props;

    return(
        <Col lg={6} sm={24}>
            <Row className="mb-30 mt-15">
                <Col lg={20} className="timer">
                    <Progress type="circle" percent={(timeLeft / duration) * 100} format={() => `${moment.utc(timeLeft).format('mm:ss')}`} />
                </Col>
            </Row>
            <Row className="mb-30">
                <Col lg={20} sm={24}>
                    {
                        testQuestions != null ? testQuestions.map((value, index) => {
                            return(
                                <Col key={index} xxl={6} xl={6} lg={6} md={2} sm={3} xs={6} className="testQuestionBtn-container">
                                    <Button type={currentTest === index ? 'primary' : !testQuestions[index].answer ? 'dashed' : 'primary'}
                                            ghost={currentTest === index ? false : !!testQuestions[index].answer}
                                            className="testQuestionBtn"
                                            onClick={() => getTest(index)} >{index + 1}</Button>
                                </Col>
                            );
                        }) : null
                    }
                </Col>
            </Row>
            <Row className="align-center mb-30">
                <Col lg={20} sm={24}>
                    <Button type="primary" icon="check" onClick={finishExam}>
                        İmtahanı bitir
                    </Button>
                </Col>
            </Row>
            <Row className="mb-30">
                <h3>İmtahan qaydaları:</h3>
                Sizin yazmalıq {totalCount} sualınız və {moment(duration).format('m')} dəqiqə vaxtınız var
            </Row>
        </Col>
    )
};

export default TestInfo;