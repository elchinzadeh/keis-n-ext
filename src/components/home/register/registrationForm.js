import React from 'react';
import { Card } from 'antd';

// Components
import CitizenshipSelection from './citizenshipSelection'


// Assets
import './register.css';


export default class RegistrationForm extends React.Component {

    constructor() {
        super();

        this.state = {
            citizenship: '',
        }

    }

    render () {
        return (
            <div className="registrationForm">
                <Card bordered={false}>
                    <CitizenshipSelection/>
                </Card>
            </div>
        )
    }
}