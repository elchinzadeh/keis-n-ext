import React from 'react';
import { Col, Row, Card, Button, Divider } from 'antd';

import azmap from './azerbaijan_map.svg';
import worldmap from './earth_map.svg'

export default class CitizenshipSelection extends React.Component {

    constructor() {
        super();

        this.state = {
            citizenship: ''
        }
    }

    render() {
        const   iconStyle = {
                    width: '135px',
                },
                cardStyle = {
                    textAlign: 'center'
                };

        return (
            <React.Fragment>
                <Row>
                    <Divider>
                        <h2 className="title">Vətəndaşlığınızı seçin</h2>
                    </Divider>
                </Row>
                <Row type="flex" justify="center" >
                    <Col sm={24} md={16} lg={14} xl={10} xxl={82} >
                        <Row>
                            <Col sm={24} md={12}>
                                <Card bordered={false} style={cardStyle}>
                                    <img src={azmap} alt="" style={iconStyle} />
                                    <h3>Azərbaycan vətəndaşı</h3>
                                </Card>
                            </Col>
                            <Col sm={24} md={12}>
                                <Card bordered={false} style={cardStyle}>
                                    <img src={worldmap} alt="" style={iconStyle} />
                                    <h3>Əcnəbi vətəndaş</h3>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row type="flex" justify="center">
                    <Button type="primary">Davam et</Button>
                </Row>
            </React.Fragment>
        )
    }
};