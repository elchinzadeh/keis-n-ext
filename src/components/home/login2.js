import React, {Component} from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';

const FormItem = Form.Item;

class NormalLoginForm extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                <h1>Daxil ol</h1>
                <FormItem>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Zəhmət olmasa, emailinizi daxil edin' }],
                    })(
                        <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Zəhmət olmasa, şifrənizi daxil edin' }],
                    })(
                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Şifrə" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('remember', {
                        valuePropName: 'checked',
                        initialValue: true,
                    })(
                        <Checkbox>Məni xatırla</Checkbox>
                    )}
                    <a className="login-form-forgot" href="">Şifrəni unutmuşam</a>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Daxil ol
                    </Button>
                    Və ya <a href="">qeydiyyatdan keç!</a>
                </FormItem>
            </Form>
        );
    }
}

const LoginForm = Form.create()(NormalLoginForm);

export default Login;