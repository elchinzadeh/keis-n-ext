import React, { Component } from 'react';
import { Col, Button, Form, Select, message } from 'antd';

import './selectCenterShift.css'
import CenterAndShiftSelectionApi from "../../../../api/master1/process/centerAndShiftSelectionApi";

const FormItem = Form.Item;
const Option = Select.Option;

class SelectCenterShiftForm extends Component{
    constructor(props){
        super(props);

        this.state = {
            centerList: [],
            shiftList: [],
            selectedShift: null
        }
    }

    componentDidMount(){
        this.getCenterList()
    }

    getCenterList = () => {
        CenterAndShiftSelectionApi.getCenters().then(response => {
            if (response.data.error === null) {
                this.setState({
                    centerList: response.data.data.entities
                })
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    getShiftList = (id) => {
        CenterAndShiftSelectionApi.getShiftsByCenterId(id).then(response => {
            if (response.data.error === null){
                this.setState({
                    shiftList: response.data.data.entities
                })
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    handleShiftChange = (e) => {
        this.setState({
            selectedShift: e
        })
    };

    handleSubmit = () => {
        let body = {
            relCenterShiftId: this.state.selectedShift
        };

        CenterAndShiftSelectionApi.submitCenterAndShift(body).then(response => {
            if (response.data.error === null){
                message.success('Mərkəz və növbə seçimi tamamlandı');
                this.props.getStatus();
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    render() {
        const { getFieldDecorator } = this.props.form;

        return(
            <Col className="mt-30" lg={{ span: 12, offset: 6}} md={{ span: 16, offset: 4}}>
                <Form>
                    <FormItem>
                        {getFieldDecorator('center', {
                            rules: [{ required: true }],
                        })(
                            <Select
                                showSearch
                                placeholder="Mərkəzi seçin"
                                optionFilterProp="name"
                                onChange={value => this.getShiftList(value)}
                            >
                                {this.state.centerList.map((value) => {
                                    return <Option value={value.id} name={value.name} key={value.id}>{value.name}</Option>
                                })}
                            </Select>
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('shift', {
                            rules: [{ required: true }],
                        })(
                            <Select
                                placeholder="Növbəni seçin"
                                onChange={this.handleShiftChange}
                            >
                                {this.state.shiftList.map((value) => {
                                    return <Option value={value.id} key={value.id}>{value.name}</Option>
                                })}
                            </Select>
                        )}
                    </FormItem>
                    <FormItem>
                        <Button type="primary" onClick={this.handleSubmit}>
                            Təsdiqlə
                        </Button>
                    </FormItem>
                    <p>Təsdiqlədikdən sonra dəyişə bilməyəcəksiniz</p>
                </Form>
            </Col>
        )
    }
}

const SelectCenterShift = Form.create()(SelectCenterShiftForm);

export default SelectCenterShift;