import React, { Component } from 'react';
import { Card, Button, Icon } from 'antd';
import TestApi from "../../../../api/general/testApi";

export default class TestFirstFailed extends Component{

    constructor() {
        super();

        this.state = {
            fetching: false
        };
    }

    componentDidMount() {
        this.checkActiveTest()
    }

    checkActiveTest = () => {
        this.setState({
            fetching: true
        });

        TestApi.checkActiveTest().then(response => {
            if(response.data.error === null) {
                if(response.data.data === 'yes') {
                    this.showTest();
                }else if(response.data.data === 'no') {
                    this.setState({
                        fetching: false
                    })
                }else if(response.data.data === 'expired') {
                    //
                }
            }
        });
    };

    showTest = () => {
        this.setState({
            fetching: false
        });
        this.props.startTest();
    };

    render() {
        return(
            <Card className="card" bordered={false} align="center" loading={this.state.fetching}>
                <h1><Icon type="frown-o"/></h1>
                <h2>Siz imtahan mərhələsində birinci dəfədə uğursuz oldunuz</h2>
                <p>Amma sizin 1 şansınız daha var <Icon type="smile-o"/></p>
                <Button type="primary" onClick={this.showTest}>Testə başla</Button>
            </Card>
        )
    }
}