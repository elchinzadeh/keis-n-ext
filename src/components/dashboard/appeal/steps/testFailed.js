import React, { Component } from 'react';
import { Card, Icon } from 'antd';

export default class TestFailed extends Component{
    render() {
        return(
            <Card className="card" bordered={false} align="center">
                <h1><Icon type="frown-o"/></h1>
                <h2>Siz imtahan mərhələsində uğursuz oldunuz</h2>
                <p>Daha sonra yenidən yoxlayın <Icon type="smile-o"/></p>
            </Card>
        )
    }
}