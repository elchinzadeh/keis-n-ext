import React, { Component } from 'react';
import { Card, Icon} from 'antd';

import SelectInterviewDate from '../selectInterviewDate/selectInterviewDate';

export default class InterviewCenterAndShiftSelected extends Component{
    render() {

        return(
            <Card className="card" bordered={false} align="center">
                <h1><Icon type="smile-o"/></h1>
                <h2>Mərkəz və növbə seçimi tamamladı</h2>
                <p>Lorem İpsum</p>
                <SelectInterviewDate/>
            </Card>
        )
    }
}