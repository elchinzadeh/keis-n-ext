import React, { Component } from 'react';
import { Button, Collapse } from 'antd';

import { DashboardContext } from "../../../../contexts/dashboardContext";

import './selectInterviewDate.css'

const Panel = Collapse.Panel;

export default class SelectCenterShiftForm extends Component{


    render() {
        const text = 'halooooo';
        const customPanelStyle = {
            background: '#f7f7f7',
            borderRadius: 4,
            marginBottom: 24,
            border: 0,
            overflow: 'hidden',
        };

        return (
                <DashboardContext.Consumer>
                    {value =>
                            <React.Fragment>
                                <h3>Müsahibə vaxtını seçin</h3>

                                <Collapse bordered={false} defaultActiveKey={['1']}>
                                    <Panel md={12} header="This is panel header 1" key="1" style={customPanelStyle}>
                                        <p>{text}</p>
                                    </Panel>
                                    <Panel header="This is panel header 2" key="2" style={customPanelStyle}>
                                        <p>{text}</p>
                                    </Panel>
                                    <Panel header="This is panel header 3" key="3" style={customPanelStyle}>
                                        <p>{text}</p>
                                    </Panel>
                                </Collapse>

                                <Button type="primary" onClick={() => value.getStatus()}>Davam et</Button>
                            </React.Fragment>
                    }
                </DashboardContext.Consumer>

        )
    }
}