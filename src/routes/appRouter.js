import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Attendance from '../pages/attendance/attendance';
import Dashboard from '../pages/dashboard/dashboard';
import Home from '../pages/home/home';
import Home2 from '../pages/home/home2';
import Registration from '../pages/home/registration/registration';
import NotFound from '../pages/notFound/notFound';
import Profile from '../pages/profile/profile';

import ExtLogin from '../ext-components/login'

const AppRouter = props => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route path="/2" component={Home2}/>
                <Route path="/registration" component={Registration}/>
                <Route path="/asanKadr/login" component={ExtLogin}/>
                <Route path="/attendance" component={Attendance}/>
                <Route path="/dashboard" component={Dashboard}/>
                <Route path="/profile" component={Profile}/>
                <Route component={NotFound}/>
            </Switch>
        </BrowserRouter>
    );
};

export default AppRouter;
