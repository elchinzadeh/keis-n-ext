import React, {Component} from 'react';

import PageWrapper from '../../hoc/pageWrapper';
import Attendance from '../../components/attendance/attendance'

export default class Profile extends Component {

    render() {

        return (
            <PageWrapper currentRoute={this.props.location.pathname}>
                <Attendance/>
            </PageWrapper>
        );

    }

}

