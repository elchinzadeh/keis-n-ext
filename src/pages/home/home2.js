import React, {Component} from 'react';
import { Card, Col, Form, Icon, Input, Button, Checkbox } from 'antd';

import './home2.css';
import logo from '../../logo-b.png';
const FormItem = Form.Item;


export default class Home extends Component {



    render() {
        return (
            <div className="page-wrapper gradient-bg new-login-form ">
                <Card className="radiusless-card " bordered={false}>
                    <Col sm={24} md={12} className="infographic">
                        <Card className="transparent-bg radiusless-card infographic-body" align="center">
                            <h1 className="title">ASAN Könüllülük Məktəbi</h1>
                            <p className="description">Azərbaycan Respublikasının Prezidenti yanında Vətəndaşlara Xidmət və Sosial İnnovasiyalar üzrə Dövlət Agentliyinin tabeliyində olan “ASAN xidmət” mərkəzlərində könüllü fəaliyyət göstərmək istəyən şəxslər ilk növbədə qeydiyyatdan keçməlidir.</p>
                            <a className="link" href="/">Qeydiyyatdan keç</a>
                        </Card>
                    </Col>
                    <Col sm={24} md={12} className="signin-form" align="center">
                        <img src={logo} alt=""/>
                        <Form onSubmit={this.handleSubmit} className="login-form" align="left">
                            <FormItem>

                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                            </FormItem>
                            <FormItem>
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                            </FormItem>
                            <FormItem>
                                    <Checkbox>Remember me</Checkbox>
                                <a className="login-form-forgot" href="">Forgot password</a>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Log in
                                </Button>
                                Or <a href="/">register now!</a>
                            </FormItem>
                        </Form>
                    </Col>
                </Card>
            </div>
        );
    }
}

// const WrappedNormalLoginForm = Form.create()(Form);
