import React, {Component} from 'react';
import { Col } from 'antd';
import './home.css';
import logo from '../../logo-w.png';
import LoginForm from '../../components/home/loginForm';
import SearchCertificate from '../../components/home/searchCertificate';

export default class Home extends Component {

    render() {
        return (
            <div className="page-wrapper">
                    <Col sm={24} xl={12}>
                        <div className="page-left">
                            <div className="logo-container">
                                <img src={logo} className="logo" alt=""/>
                            </div>
                            <div className="content">
                                <h1>ASAN Könüllülük Məktəbi</h1>
                                <p>
                                    Azərbaycan Respublikasının Prezidenti yanında Vətəndaşlara Xidmət və Sosial İnnovasiyalar üzrə Dövlət Agentliyinin tabeliyində
                                    olan “ASAN xidmət” mərkəzlərində könüllü fəaliyyət göstərmək istəyən şəxslər ilk növbədə qeydiyyatdan keçməlidir.
                                </p>
                                <SearchCertificate/>
                            </div>
                        </div>
                    </Col>
                    <Col sm={24} xl={12}>
                        <div className="page-right">
                        <div className="right-form-container">
                            <LoginForm/>
                        </div>
                    </div>
                    </Col>
            </div>
        );
    }
}