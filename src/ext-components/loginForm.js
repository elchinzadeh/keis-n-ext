import React, {Component} from 'react';
import { Redirect } from 'react-router-dom'

import { Form, Icon, Input, Button, message } from 'antd';
import cookie from 'react-cookies';
import ExternalApi from "../api/externalApi";

const FormItem = Form.Item;

class NormalLoginForm extends Component {

    state = {
        tokenAsanKadr: '',
        emailAsanKadr: '',
        redirect: false
    };

    renderRedirect = () => {
        const   url_string = window.location.href,
                url = new URL(url_string),
                callbackUrl = url.searchParams.get("callbackUrl"),
                callbackToken = this.state.tokenAsanKadr,
                callbackemail = this.state.emailAsanKadr;
        const requestUrl = callbackUrl+'?token='+callbackToken+'&email='+callbackemail;

        if (this.state.redirect) {
            window.location = requestUrl;
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const body = {
                    email: values.email,
                    password: values.password
                };

                ExternalApi.loginForAsanKadr(body).then(response => {
                    if (response.data.error === null) {
                        cookie.save('userToken', response.data.data.token, { path: '/' });
                        this.setState({
                            tokenAsanKadr: response.data.data.tokenAsanKadr,
                            emailAsanKadr: response.data.data.email,
                            redirect: true
                        })
                    }else {
                        message.error(response.data.error.message)
                    }
                });
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <React.Fragment>
                {this.renderRedirect()}
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <h1>Daxil ol</h1>
                    <FormItem>
                        {getFieldDecorator('email', {
                            rules: [{ type: 'email', message: 'Zəhmət olmasa, dzgün email adresi daxil edin' },
                                { required: true, message: 'Zəhmət olmasa, email adresinizi daxil edin' }],
                        })(
                            <Input type="email" prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Zəhmət olmasa, şifrənizi daxil edin' }],
                        })(
                            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Şifrə" />
                        )}
                    </FormItem>
                    <FormItem>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Daxil ol
                        </Button>
                    </FormItem>
                </Form>
            </React.Fragment>

        );
    }
}

const LoginForm = Form.create()(NormalLoginForm);

export default LoginForm;