import React, {Component} from 'react';
import AppRouter from './routes/appRouter';

import 'moment/locale/az';
import './App.css';

class App extends Component {
    render() {
        return <AppRouter/>;
    }
}

export default App;
