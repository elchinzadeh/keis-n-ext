const axios = require('axios/index');
const express = require('express');
const methodOverride = require('method-override');
const router = express.Router();



app = express();

app.use(methodOverride('X-HTTP-Method'));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(methodOverride('X-Method-Override'));

router.all('*', (req, res) => {
    const url = 'http://konullu.asanschool.az/api' + req.url;
    const headers = {
        'Accept-Language': 'az',
        'Content-Type': 'application/json',
    };
    let data = null;
    if (req.method === 'POST' || req.method === 'PUT') {
        data = req.body;
    }

    axios({
        url: url,
        method: req.method,
        data: data,
        responseType: 'json',
        headers: headers,
    }).then(response => {
        res.json(response.data)
    }).catch(err => {
        console.log(err)
    });
});

module.exports = router;