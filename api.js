/**
 * Port of Server
 * Be cautios if this PORT will updated change respectively all request in apps
 * @type {number}
 */
const PORT = 4200;
/**
 * Main Api endpoint for requests
 * Be cautios if this ENDPOINT will updated change respectively all request in apps
 * @type {string}
 */
const ENDPOINT = '/';
/**
 * Node Express Application Requirer
 * @type {*|createApplication}
 */
const EXPRESS = require('express');
/**
 * Express Application Initializer
 */
const APP = EXPRESS();
/**
 * Axios HTTP Reqeust Client
 * Make Request
 * @type {{AxiosTransformer: AxiosTransformer; AxiosAdapter: AxiosAdapter; AxiosBasicCredentials: AxiosBasicCredentials; AxiosProxyConfig: AxiosProxyConfig; AxiosRequestConfig: AxiosRequestConfig; AxiosResponse: AxiosResponse; AxiosError: AxiosError; AxiosPromise: AxiosPromise; CancelStatic: CancelStatic; Cancel: Cancel; Canceler: Canceler; CancelTokenStatic: CancelTokenStatic; CancelToken: CancelToken; CancelTokenSource: CancelTokenSource; AxiosInterceptorManager: AxiosInterceptorManager; AxiosInstance: AxiosInstance; AxiosStatic: AxiosStatic}}
 */
const axios = require('axios');
/**
 * Parse HTTP Request Body
 * @type {Parsers|*}
 */
const bodyParser = require('body-parser');
/**
 * Parse Body as JSON
 */
const parserConfig = bodyParser.json({limit: '10mb'});
/**
 * Parse Body as URL Encoded
 */
// const parserConfig = bodyParser.urlencoded({ extended: false });

/**
 * CORS Handling Middleware
 * @param req
 * @param res
 * @param next
 * @constructor
 */
const CORS = (req, res, next) => {
    let headers = '';
    if (typeof req.headers['access-control-request-headers'] !== 'undefined') {
        headers += req.headers['access-control-request-headers'];
        headers += ', Content-Type, content-type'
    } else {
        headers = '*'
    }
    res.set({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,OPTIONS',
        'Access-Control-Allow-Headers': headers
    });
    next();
}
/**
 * Middleware
 */
APP.use(CORS);
APP.use(bodyParser.urlencoded({ extended: true }))

/**
 * Get Request
 */
APP.get(ENDPOINT, (req, res) => {
    const headers = headerGenerator(req.rawHeaders);
    const URL = typeof req.query.url !== 'undefined' && req.query.url !== null ? req.query.url : '';
    const queryParams = req.query;
    const params = paramGenerator(queryParams);
    let axiosOptions = {
        method: 'GET',
        url: URL+params,
        headers: headers,
        json: true
    };
    axios(axiosOptions)
        .then(response => {
            sendBackResponse(response.data);
        })
        .catch(error => {
            sendBackError(error.message);
        });

    /**
     * Return Response
     * @param response
     */
    function sendBackResponse(response){
        responser(res, response);
    }

    /**
     * Return Error Message
     * @param error
     */
    function sendBackError(error){
        exceptionHandler(res, error);
    }
});

/**
 * POST Request
 */
APP.post(ENDPOINT, parserConfig, (req, res) => {
    let body = '';
    const headers = headerGenerator(req.rawHeaders);
    const queryParams = req.query;
    const params = paramGenerator(queryParams);
    const URL = typeof req.query.url !== 'undefined' && req.query.url !== null ? req.query.url : '';
    let axiosOptions = {
        method: 'POST',
        url: URL+params,
        data: req.body,
        headers:headers,
        json: true
    };

    axios(axiosOptions)
        .then(response => {
            sendBackResponse(response.data);
        })
        .catch(error => {
            sendBackError(error.message);
        });
    /**
     * Return Response
     * @param response
     */
    function sendBackResponse(response){
        responser(res, response);
    }

    /**
     * Return Error Message
     * @param error
     */
    function sendBackError(error){
        exceptionHandler(res, error);
    }
});


/**
 * Put Request
 */
APP.put(ENDPOINT, parserConfig, (req, res) => {
    let body = '';
    const headers = headerGenerator(req.rawHeaders);
    const queryParams = req.query;
    const params = paramGenerator(queryParams);
    const URL = typeof req.query.url !== 'undefined' && req.query.url !== null ? req.query.url : '';
    let axiosOptions = {
        method: 'PUT',
        url: URL+params,
        data: req.body,
        headers: headers,
        json: true
    };
    axios(axiosOptions)
        .then(response => {
            sendBackResponse(response.data);
        })
        .catch(error => {
            sendBackError(error.message);
        });
    /**
     * Return Response
     * @param response
     */
    function sendBackResponse(response){
        responser(res, response);
    }

    /**
     * Return Error Message
     * @param error
     */
    function sendBackError(error){
        exceptionHandler(res, error);
    }
});


/**
 * DELETE Request
 */
APP.delete(ENDPOINT, (req, res) => {
    const headers = headerGenerator(req.rawHeaders);
    const queryParams = req.query;
    const params = paramGenerator(queryParams);
    const URL = typeof req.query.url !== 'undefined' && req.query.url !== null ? req.query.url : '';
    let axiosOptions = {
        method: 'DELETE',
        url: URL+params,
        headers: headers,
        json: true
    };
    axios(axiosOptions)
        .then(response => {
            sendBackResponse(response.data);
        })
        .catch(error => {
            sendBackError(error.message);
        });
    /**
     * Return Response
     * @param response
     */
    function sendBackResponse(response){
        responser(res, response);
    }

    /**
     * Return Error Message
     * @param error
     */
    function sendBackError(error){
        exceptionHandler(res, error);
    }
});
/**
 * Download File
 */
APP.get(ENDPOINT+'download', (req, res) => {
    const URL = typeof req.query.url !== 'undefined' && req.query.url !== null ? req.query.url : '';
    const queryParams = req.query;
    const params = paramGenerator(queryParams, 1);
    const headers = {
        TableName: req.query.tn,
        'Accept-Language': 'az',
        Authorization: 'Basic '+req.query.token,
        IpAddress: '192.168.35.12',
        'Content-Type': 'application/json',
        UserGroupTypeId: ''+req.query.ut+'',
        AppName: 'HR',
        requestNumber: 'lLCK!-8pWtu-e4Dat-wO%tp'
    }

    let axiosOptions = {
        method: 'GET',
        url: URL+params,
        headers: headers,
        json: true
    };
    axios(axiosOptions)
        .then(response => {
            sendBackResponse(response.data,req);
        })
        .catch(error => {
            sendBackError(error.message);
        });

    /**
     * Return Response
     * @param response
     */
    function sendBackResponse(response,req){
        res.set('Content-Disposition', `attachment; filename="download.${req.query.ext.toLowerCase()}"`);
        res.set('Content-Type', req.query.mime);
        res.end(response.data[0].encodedFile, 'base64');
    }

    /**
     * Return Error Message
     * @param error
     */
    function sendBackError(error){
        exceptionHandler(res, error);
    }
});

// ----------------------- Helpers -------------------------
/**
 * If one of following codes occured logout from system
 * @type {number[]}
 */
const logoutCodes = [
    401,  // Bu ünvana icazəniz yoxdur
    1005, // You are not allowed to request this resource
    1006, // Token tapılmadı
    1014, // Token tapılmadı, zəhmət olmasa sistemə yenidən daxil olun
    1015, // Sizin sistemlərdən istifədə hüququnuz yoxdur
    1016, // Sizin bu sistemə istifədə hüququnuz yoxdur
    1017, // Sessiya bağlanmışdır, zəhmət olmasa sistemə yenidən daxil olun.
    1401, // Sessiya müddəti bitmişdir, zəhmət olmazsa sistemə yenidən daxil olun
    // 1402  // Sizin bu servisə icazəniz yoxdur
]
/**
 * Response to request
 * @param response
 * @param requestData
 */
function responser(response, requestData){
    if (typeof requestData.error === 'undefined' || typeof requestData.error !== 'undefined' && requestData.error === null) {
        response.status(200).send(requestData);
    } else if(typeof requestData.error !== 'undefined') {
        if (logoutCodes.indexOf(requestData.error.code) !== -1 ) {
            response.status(401).send(requestData.error.message);
        } else {
            response.status(500).send(requestData.error.message);
        }
    }
}

/**
 * Response error
 * @param response
 * @param errorMSG
 */
function exceptionHandler(response, errorMSG) {
    response.status(500).send(errorMSG);
}
/**
 * Fix Headers
 * @param headers
 */
function headerGenerator(headers) {
    const defaultHeaders = [
        'Accept',
        'Pragma',
        'Accept-Encoding' ,
        'Connection',
        'Cookie' ,
        'DNT' ,
        'Host' ,
        'Referer' ,
        'User-Agent' ,
        'Cache-Control' ,
        'Date' ,
        'Server',
        'Transfer-Encoding',
        'X-Powered-By',
        'If-None-Match'
    ];
    const keys = [];
    const values = [];
    const newHeaders = {};
    headers.forEach((value , index) => {
        if( index == 0 || index % 2 === 0) {
            keys.push(value);
        } else {
            values.push(value);
        }
    })
    const count = keys.length;
    for (let i = 0; i < count; i++) {
        const newKey = keys[i];
        const newValue = values[i];
        if(defaultHeaders.indexOf(newKey) === -1){
            newHeaders[newKey] = newValue;
        }
    }
    return newHeaders;
}

/**
 * Fetch Headers for CORS
 * @param headers
 * @returns {string}
 */
function getRequestHeadersString(headers) {
    let response = '';
    const defaultHeaders = [
        'accept',
        'pragma',
        'accept-encoding' ,
        'connection',
        'cookie' ,
        'dnt' ,
        'host' ,
        'referer' ,
        'user-agent' ,
        'cache-control' ,
        'date' ,
        'server',
        'transfer-encoding',
        'X-Powered-By',
        'If-None-Match'
    ];
    const keylist = Object.keys(headers);
    const keylistCount = keylist.length;
    for (let i = 0; i < keylistCount; i++) {
        if(defaultHeaders.indexOf(keylist[i]) === -1){
            if((i+1) < keylistCount){
                response += keylist[i]+', '
            } else {
                response += keylist[i]
            }
        }
    }
    return response;
}
/**
 * Params Generator
 * @param queryParams
 * @returns {string|string}
 */
function paramGenerator(queryParams, download = null) {
    let params = '';
    for (var key in queryParams) {
        if (queryParams.hasOwnProperty(key)) {
            if(key === 'paths' && download !== null) {
                params += key + '=["'+encodeURIComponent(queryParams[key])+'"]'
            }
            else if(key !== 'url') {
                params += key+'='+encodeURIComponent(queryParams[key])+'&';
            }
        }
    };

    params = params.substring(0, params.length - 1);
    params = '?'+params;
    if (params === '?') {
        params = '';
    }
    return params;
}