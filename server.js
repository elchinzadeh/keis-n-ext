const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');

const api = require('./api-demo');
const app = express();
app.use(cors());

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'build')));

app.use('/api', api);

app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'build/index.html'));
});

const server = app.listen(80, () => console.log('Server running on %s:%s', server.address().address, server.address().port));