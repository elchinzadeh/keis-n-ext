const axios = require('axios/index');
const express = require('express');
const router = express.Router();

app = express();

// G E T   M E T H O D
router.get('*', (req, res) => {
    const url = 'http://konullu.asanschool.az/api' + req.url;
    const headers = {
        'Accept-Language': 'az',
        'Content-Type': 'application/json',
    };
    axios({
        url: url,
        method: 'GET',
        responseType: 'json',
        headers: headers,
    }).then(response => { res.json(response.data) })
    .catch(err       => { console.log(err) });
});

// P O S T   M E T H O D
router.post('*', (req, res) => {
    const url = 'http://konullu.asanschool.az/api' + req.url;
    const headers = {
        'Accept-Language': 'az',
        'Content-Type': 'application/json',
    };
    const data = req.body;
    axios({
        url: url,
        method: 'POST',
        data: data,
        responseType: 'json',
        headers: headers,
    }).then(response => { res.json(response.data) })
    .catch(err       => { console.log(err) });
});

// P U T   M E T H O D
router.put('*', (req, res) => {
    const url = 'http://konullu.asanschool.az/api' + req.url;
    const headers = {
        'Accept-Language': 'az',
        'Content-Type': 'application/json',
        'X-HTTP-Method-Override': 'PUT'
    };
    const data = req.body;
    axios({
        url: url,
        method: 'PUT',
        data: data,
        responseType: 'json',
        headers: headers,
    }).then(response => { res.json(response.data) })
        .catch(err       => { console.log(err) });
});

// D E L E T E   M E T H O D
router.delete('*', (req, res) => {
    const url = 'http://konullu.asanschool.az/api' + req.url;
    const headers = {
        'Accept-Language': 'az',
        'Content-Type': 'application/json',
        'X-HTTP-Method-Override': 'DELETE'
    };
    axios({
        url: url,
        method: 'DELETE',
        responseType: 'json',
        headers: headers,
    }).then(response => { res.json(response.data) })
        .catch(err       => { console.log(err) });
});

module.exports = router;